﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace biblioteka
{
    public partial class Form4 : Form
    {
        MySqlConnection veza = new MySqlConnection("SslMode = none; datasource = localhost; port = 3306; username = root; password = root");
        MySqlDataAdapter adapter;
        MySqlCommand komanda;
        DataTable tabla;

        Int32 ID;

        public Form4()
        {
            InitializeComponent();
            loadDataGrid();
        }

        private void btn_return_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 form_meni = new Form1();
            form_meni.Show();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loadDataGrid()
        {
            string select_query = "SELECT * FROM biblioteka.rezervacije";
            adapter = new MySqlDataAdapter();
            komanda = new MySqlCommand(select_query, veza);
            tabla = new DataTable();
            BindingSource bs = new BindingSource();

            veza.Open();
            adapter.SelectCommand = komanda;
            adapter.Fill(tabla);
            bs.DataSource = tabla;

            dataGridView1.DataSource = bs;
            veza.Close();
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            ID = Int32.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            textBoxClan.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            textBoxKnjiga.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            dateTimePicker1.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            string query_update = "UPDATE biblioteka.rezervacije SET Clan = '" + textBoxClan.Text + "', Knjiga = '" + textBoxKnjiga.Text + "', Datum =  STR_TO_DATE('" + dateTimePicker1.Value.Date.ToString("dd-MM-yyyy") + "', '%d-%m-%Y')";
            komanda = new MySqlCommand(query_update, veza);

            veza.Open();
            komanda.ExecuteNonQuery();
            veza.Close();
            loadDataGrid(); // Refreshujemo prikaz tabele
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            string query_insert = "INSERT INTO biblioteka.rezervacije(Clan, Knjiga, Datum) VALUES ('" + this.textBoxClan.Text + "', '" + this.textBoxKnjiga.Text + "', STR_TO_DATE('" + dateTimePicker1.Value.Date.ToString("dd-MM-yyyy") + "', '%d-%m-%Y'))";
            komanda = new MySqlCommand(query_insert, veza);

            veza.Open();
            komanda.ExecuteNonQuery();
            veza.Close();
            loadDataGrid(); // Refreshujemo prikaz tabele
        }

        private void btn_remove_Click(object sender, EventArgs e)
        {
            string query_remove = "DELETE FROM biblioteka.rezervacije WHERE RezervacijaID IN ('" + ID + "')";
            komanda = new MySqlCommand(query_remove, veza);

            veza.Open();
            komanda.ExecuteNonQuery();
            veza.Close();
            loadDataGrid();
        }

        void SearchData(string valueToSearch)
        {
            string query_trazi = "SELECT * FROM biblioteka.rezervacije WHERE CONCAT (Clan, Knjiga, Datum) LIKE '%" + valueToSearch + "%'";
            adapter = new MySqlDataAdapter(query_trazi, veza);
            tabla = new DataTable();
            adapter.Fill(tabla);
            dataGridView1.DataSource = tabla;
        }

        private void textBoxTrazi_TextChanged(object sender, EventArgs e)
        {
            SearchData(textBoxTrazi.Text);
        }
    }
}
