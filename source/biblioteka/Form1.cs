using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace biblioteka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_clanovi_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form_clanovi = new Form2();
            form_clanovi.Show();
        }

        private void btn_knjige_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 form_knjige = new Form3();
            form_knjige.Show();
        }
    }
}
