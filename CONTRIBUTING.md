## Kako doprineti
- Sve izmene ce se desavati na master branch
- Posle dovoljno testiranja master branch-e programa promene ce se merge-ovati(spojiti) sa stable branch-om.
- Mozete slobodno menjati kod (commits), fork-ovati projekat (kopirati ga na svoj nalog) i otvarati issues (pitanja).